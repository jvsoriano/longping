class CustomPipe(object):
  def load(self, app):
    app.template_filter("tableHeader") (self.tableHeader)
    app.template_filter("currencyFormat") (self.currencyFormat)

  def tableHeader(self, header):
    return header.title().replace("_"," ")

  def currencyFormat(self, amount):
    if type(amount) == float:
      return "{:,.2f}".format(amount)
    else:
      return amount