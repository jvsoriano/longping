from flask import Flask, current_app

# For Linux
# from flask_mysqldb import MySQL

# For Windows
from flaskext.mysql import MySQL

import unicodedata


app = Flask(__name__)

with app.app_context():
  mysql = MySQL(current_app)

class Common(object):
  def fetch_all(self, query_str):
    # Initialize return
    retval = {}

    # Create cursor
    # cur = mysql.connection.cursor()
    conn = mysql.connect()
    cur = conn.cursor()

    # Execute query
    result = cur.execute(query_str)

    # Fetchall
    data = cur.fetchall()

    if result > 0:
      data = self.create_response(data, cur)
      retval["status"] = "success"
      retval["data"] = data
    else:
      msg = "No available data."
      retval["status"] = "failed"
      retval["msg"] = msg

    # Close cursor
    cur.close()

    # Return
    return retval

  def fetch(self, query_str):
    # Initialize return
    retval = {}

    # Create cursor
    # cur = mysql.connection.cursor()
    conn = mysql.connect()
    cur = conn.cursor()

    # Execute query
    result = cur.execute(query_str)

    # Fetchone
    data = cur.fetchone()

    if result > 0:
      data = self.create_response([data], cur)[0]
      retval["status"] = "success"
      retval["data"] = data
    else:
      msg = "No available data."
      retval["status"] = "failed"
      retval["msg"] = msg

    # Close cursor
    cur.close()

    # Return
    return retval

  def commit(self, query_str):
    # Create cursor
    # cur = mysql.connection.cursor()
    conn = mysql.connect()
    cur = conn.cursor()

    # Execute query
    cur.execute(query_str)

    # Commit
    # mysql.connection.commit()
    conn.commit()

    # Close cursor
    cur.close()

  def get_added_by(self, data):
    # Create cursor
    # cur = mysql.connection.cursor()
    conn = mysql.connect()
    cur = conn.cursor()

    for d in data:
      query_str = "SELECT username FROM users WHERE id = {}".format(d["added_by"])

      # Execute query
      result = cur.execute(query_str)

      # Fetchone
      user = cur.fetchone()
      user = self.create_response([user], cur)[0]

      d["added_by_id"] = d["added_by"]
      d["added_by"] = user["username"]

    # Close cursor
    cur.close()

    return data

  def get_product_name(self, data):
    # Create cursor
    # cur = mysql.connection.cursor()
    conn = mysql.connect()
    cur = conn.cursor()

    for d in data:
      query_str = "SELECT name FROM products WHERE id = {}".format(d["product_id"])

      # Execute query
      result = cur.execute(query_str)

      # Fetchone
      product = cur.fetchone()
      product = self.create_response([product], cur)[0]

      d["product"] = product["name"]

    # Close cursor
    cur.close()

    return data

  def create_response(self, db_data, cur):
    # Initialize return variable
    data = []

    # Get columns
    cols = self.fetch_columns(cur)

    # Create return
    for rec in db_data:
      hash = {}
      for i, col in enumerate(cols):

        # Check if value has unicode
        if isinstance(rec[i], unicode):
          nfkd_form = unicodedata.normalize('NFKD', rec[i])
          only_ascii = nfkd_form.encode('ASCII', 'ignore')
          hash[str(col)] = only_ascii
        else:
          hash[str(col)] = rec[i]
      data.append(hash)

    # return
    return data

  def fetch_columns(self, cur):
    cols = cur.description
    cols = [str(col[0]) for col in cols]
    return cols
