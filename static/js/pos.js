function addPosProduct(product) {
  var htmlString = "<tr id='"+product.id+"'>";
  htmlString += "<td><input type='text' class='form-control input-sm' name='name-"+product.id+"' value='"+product.name+"' readonly></td>";
  htmlString += "<td><input type='number' class='form-control input-sm' name='srp-"+product.id+"' value='"+product.srp.toFixed(2)+"' readonly></td>";
  htmlString += "<td><input type='number' class='form-control input-sm' name='quantity-"+product.id+"' value='1' min='0' max='"+product.quantity+"' onkeyup='reCalculateTotal()' onchange='reCalculateTotal()'></td>";
  htmlString += "<td>";
  htmlString += "<button type='button' class='btn btn-danger btn-sm' onclick='removePosProduct("+product.id+")'>";
  htmlString += "<i class='glyphicon glyphicon-remove'></i>";
  htmlString += "</button>";
  htmlString += "</td>";
  htmlString += "</tr>";

  $("#pos-product-table > tbody:last-child")
  .append(htmlString);

  if (!$("#customer_total").val()) {
    $("#customer_total").val((product.srp).toFixed(2))
  } else {
    var currentTotal = parseFloat($("#customer_total").val());
    $("#customer_total").val((currentTotal + product.srp).toFixed(2))
  }

  $(".btn-pos-submit").prop("disabled", false);
}

function removePosProduct(id) {
  $("#"+id).remove();
  reCalculateTotal();
  calculateChange();

  if ($("#pos-product-table tr").length === 1) {
    $(".btn-pos-submit").prop("disabled", true);
  } 
}

function reCalculateTotal() {
  var customer_total = 0.00;
  $("#pos-product-table tr").each(function() {
    var qty = 0;
    var srp = 0.00;
    $(this).find('td input').each(function(idx,data) {
      if (data.name.indexOf("quantity") != -1) {
        qty = data.value;
      } else if (data.name.indexOf("srp") != -1) {
        srp = data.value;
      }
    });
    customer_total = customer_total + (qty * srp);
  });

  $("#customer_total").val(customer_total.toFixed(2))

  calculateChange();
}

function calculateChange() {
  var customer_total = parseFloat($("#customer_total").val());
  var customer_payment = parseFloat($("#customer_payment").val());
  var customer_change = customer_payment - customer_total;
  if (isNaN(customer_change)) {
    $("#customer_change").val("");
  } else {
    $("#customer_change").val(customer_change.toFixed(2));
  }
}

$(".pos-form").submit(function() {
  if (!$("#invoice_no").val() || !$("#customer_name").val() || !$("#customer_payment").val()) {
    event.preventDefault();
    alert("ERROR: Some details for the transaction is empty!");
  }
});
