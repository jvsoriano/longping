# Define main configuration
class Config(object):
  # Flask configuration
  DEBUG = True
  SECRET_KEY = "jvsoriano"

# Define MySQL configuration
class MySQLconfig(object):
  # MySQL configuration
  MYSQL_HOST = "localhost"
  MYSQL_USER = "root"
  MYSQL_PASSWORD = "root"
  MYSQL_DB = "myflaskapp"
  MYSQL_CURSORCLASS = "DictCursor"

# Define Windows MySQL configuration
class WinSQLconfig(object):
  # MySQL configuration
  MYSQL_DATABASE_HOST = "localhost"
  MYSQL_DATABASE_USER = "root"
  MYSQL_DATABASE_PASSWORD = ""
  MYSQL_DATABASE_DB = "myflaskapp"
  