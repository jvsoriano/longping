import sys

from flask import Flask, render_template, flash, redirect, url_for, session, logging, request

# For Linux
from flask_mysqldb import MySQL

# For Windows
# from flaskext.mysql import MySQL

from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from functools import wraps

# Prevent python application from writing .pyc files
sys.dont_write_bytecode = True

# Flask Application
app = Flask(__name__)

# Load flask configuration
app.config.from_object("config.Config")

# Load mysql configuration
app.config.from_object("config.SQLconfig")

# Initialize MySQL
mysql = MySQL(app)

# Custom jinja2 pipes
from lib import custom_pipes
_custom_pipes = custom_pipes.CustomPipe()
_custom_pipes.load(app)

# Prevent application to write .pyc files
sys.dont_write_bytecode = True

# Check if user is logged in
def is_logged_in(f):
  @wraps(f)
  def wrap(*args, **kwargs):
    if "logged_in" in session:
      return f(*args, **kwargs)
    else:
      flash("You are not logged in", "danger")
      return redirect(url_for("login"))
  return wrap

# Index
@app.route("/")
def index():
  return redirect(url_for("login"))
  #return render_template("home.html")

# Login
from controllers.login import login
_login = login.Login()
app.route("/login", methods=["GET", "POST"]) (_login.login)

# Logout
@app.route("/logout", methods=["GET", "POST"])
@is_logged_in
def logout():
  session.clear()
  # flash("You are now logged out", "success")

  return redirect(url_for("login"))

# Dashboard
from controllers.dashboard import dashboard
_dashboard = dashboard.Dashboard()
app.route("/dashboard", methods=["GET", "POST"]) (is_logged_in(_dashboard.dashboard))

# User
from controllers.user import user
_user = user.User()
app.route("/users", methods=["GET", "POST"]) (is_logged_in(_user.users))
app.route("/add_user", methods=["GET", "POST"]) (is_logged_in(_user.add_user))
app.route("/edit_user/<string:id>", methods=["GET", "POST"]) (is_logged_in(_user.edit_user))
app.route("/delete_user/<string:id>", methods=["GET", "POST"]) (is_logged_in(_user.delete_user))

# Product
from controllers.product import product
_product = product.Product()
app.route("/products", methods=["GET", "POST"]) (is_logged_in(_product.products))
app.route("/add_product", methods=["GET", "POST"]) (is_logged_in(_product.add_product))
app.route("/edit_product/<string:id>", methods=["GET", "POST"]) (is_logged_in(_product.edit_product))
app.route("/delete_product/<string:id>", methods=["GET", "POST"]) (is_logged_in(_product.delete_product))

# Stock
from controllers.stock import stock
_stock = stock.Stock()
app.route("/stocks", methods=["GET", "POST"]) (is_logged_in(_stock.stocks))
app.route("/add_stock", methods=["GET", "POST"]) (is_logged_in(_stock.add_stock))
#app.route("/edit_stock/<string:id>", methods=["GET", "POST"]) (is_logged_in(_stock.edit_stock))
app.route("/delete_stock/<string:id>", methods=["GET", "POST"]) (is_logged_in(_stock.delete_stock))
app.route("/stocks/monitoring", methods=["GET", "POST"]) (is_logged_in(_stock.stock_monitoring))

# POS
from controllers.pos import pos
_pos = pos.Pos()
app.route("/pos", methods=["GET", "POST"]) (is_logged_in(_pos.pos))
# app.route("/add_pos_product", methods=["GET", "POST"]) (is_logged_in(_pos.add_pos_product))

# Reports
from controllers.report import report
_report = report.Report()
app.route("/report/sales", methods=["GET"]) (is_logged_in(_report.sales))

# Sales Order
from controllers.sales_order import sales_order
_sales_order = sales_order.SalesOrder()
app.route("/sales-order", methods=["GET"]) (is_logged_in(_sales_order.sales_order))
app.route("/sales-order/add", methods=["GET", "POST"]) (is_logged_in(_sales_order.add_sales_order))



if __name__ == "__main__":
  app.run(host="0.0.0.0", debug=True)
