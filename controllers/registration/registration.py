from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, current_app
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt

app = Flask(__name__)

with app.app_context():
  mysql = MySQL(current_app)

class RegistrationForm(Form):
  name = StringField('Name', [validators.Length(min=1, max=50)])
  username = StringField('Userame', [validators.Length(min=4, max=25)])
  email = StringField('Email', [validators.Length(min=6, max=25)])
  password = PasswordField('Password', [
    validators.DataRequired(),
    validators.EqualTo('confirm', message='Passwords do not match')
  ])
  confirm = PasswordField('Confirm Password')

class Registration(object):
  def register(self):
    form = RegistrationForm(request.form)
    if request.method == "POST" and form.validate():
      name = form.name.data
      email = form.email.data
      username = form.username.data
      password = sha256_crypt.encrypt("{}".format(form.password.data))

      # Create MySQL cursor
      cur = mysql.connection.cursor()

      # Execute query
      cur.execute("INSERT INTO users (name,email,username,password) VALUES (\"{}\",\"{}\",\"{}\",\"{}\")".format(name,email,username,password))

      # Commit to MySQL
      mysql.connection.commit()

      # Close MySQL connection
      cur.close()

      flash("You are now registered and can log in", "success")

      return redirect(url_for("index"))

    return render_template("register.html", form=form)