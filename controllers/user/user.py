from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, current_app
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from lib import common

app = Flask(__name__)

cmn = common.Common()

class UserForm(Form):
  name = StringField('Name', [validators.Length(min=1, max=50)])
  username = StringField('Userame', [validators.Length(min=4, max=25)])
  email = StringField('Email', [validators.Length(min=6, max=25)])
  password = PasswordField('Password', [
    validators.DataRequired(),
    validators.EqualTo('confirm', message='Passwords do not match')
  ])
  confirm = PasswordField('Confirm Password')

class User(object):
  def users(self):
    query_str = "SELECT * FROM users"

    data = cmn.fetch_all(query_str)

    if data["status"] == "success":
      return render_template("users/users.html", users=data["data"])
    else:
      return render_template("users/users.html", msg=data["msg"])

  def add_user(self):
    form = UserForm(request.form)
    if request.method == "POST" and form.validate():
      name = form.name.data
      email = form.email.data
      username = form.username.data
      password = sha256_crypt.encrypt("{}".format(form.password.data))

      query_str = "INSERT INTO users (name,email,username,password) VALUES (\"{}\",\"{}\",\"{}\",\"{}\")".format(name,email,username,password)
      cmn.commit(query_str)

      flash("You are now usered and can log in", "success")

      return redirect(url_for("users"))

    return render_template("users/add_user.html", form=form)

  def edit_user(self, id):
    query_str = "SELECT * FROM users WHERE id={}".format(id)
    data = cmn.fetch(query_str)

    if data["status"] == "success":
      user = data["data"]
    else:
      return render_template("users/users.html", msg=data["msg"])

    form = UserForm(request.form)

    form.name.data = user["name"]
    form.username.data = user["username"]
    form.email.data = user["email"]
    form.password.data = user["password"]
    form.confirm.data = user["password"]

    if request.method == "POST" and form.validate():
      name = request.form["name"]
      username = request.form["username"]
      email = request.form["email"]
      password = sha256_crypt.encrypt("{}".format(request.form["password"]))

      query_str = "UPDATE users SET name=\"{}\",username=\"{}\",email=\"{}\",password=\"{}\" WHERE id={}".format(name,username,email,password,id)

      cmn.commit(query_str)

      flash("User Updated", "success")

      return redirect(url_for("users"))

    return render_template("users/edit_user.html", form=form)

  def delete_user(self, id):
    query_str = "DELETE FROM users WHERE id={}".format(id)
    cmn.commit(query_str)

    flash("User Deleted", "success")
    return redirect(url_for("users"))