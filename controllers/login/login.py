from flask import Flask, render_template, flash, redirect, url_for, session, logging, request
from wtforms import Form, StringField, IntegerField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from lib import common

cmn = common.Common()

class Login(object):
  def login(self):
    if request.method == "POST":
      username = request.form["username"]
      password_candidate = request.form["password"]

      query_str = "SELECT * FROM users WHERE username=\"{}\"".format(username)
      data = cmn.fetch(query_str)

      if data["status"] == "success":
        password = data["data"]["password"]

        if sha256_crypt.verify(password_candidate, password):
          session["logged_in"] = True
          session["id"] = data["data"]["id"]
          session["username"] = username

          flash("You are now logged in", "success")
          return redirect(url_for("dashboard"))
        else:
          error = "Password not matched!"
          return render_template("login.html", error=error)
      else:
        error = "Username not found!"
        return render_template("login.html", error=error)

    return render_template("login.html")
