from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, current_app
from wtforms import Form, StringField, IntegerField, FloatField, TextAreaField, PasswordField, SelectField, validators
from passlib.hash import sha256_crypt
from datetime import datetime
from lib import common

app = Flask(__name__)

cmn = common.Common()

class Report(object):

  def sales(self):
    query_str = "SELECT * FROM orders ORDER BY id DESC"

    data = cmn.fetch_all(query_str)

    if data["status"] == "success":
      data["data"] = cmn.get_added_by(data["data"])
      return render_template("reports/sales_report.html", data=data["data"])
    else:
      return render_template("reports/sales_report.html", msg=data["msg"])
