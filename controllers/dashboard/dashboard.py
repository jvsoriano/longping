from flask import Flask, render_template, flash, redirect, url_for, session, logging, request
from lib import common

cmn = common.Common()

class Dashboard(object):
  def dashboard(self):
    # Initialize data
    data = {}

    # Get total sales
    query_str = "SELECT SUM(amount) AS sales FROM orders"
    result = cmn.fetch(query_str)

    data["sales"] = result["data"]["sales"] if result["status"] == "success" else 0

    # Get total users
    query_str = "SELECT COUNT(id) AS no_of_users FROM users"
    result = cmn.fetch(query_str)

    data["users"] = result["data"]["no_of_users"] if result["status"] == "success" else 0

    return render_template("dashboard.html", data=data)
