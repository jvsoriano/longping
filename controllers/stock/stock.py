from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, current_app
from wtforms import Form, StringField, IntegerField, FloatField, TextAreaField, PasswordField, SelectField, validators
from passlib.hash import sha256_crypt
from datetime import datetime
from lib import common

app = Flask(__name__)

cmn = common.Common()

class StockForm(Form):
  product_id = SelectField("Product", coerce=int)
  capital = FloatField("Capital", [validators.NumberRange(min=1)])
  srp = FloatField("SRP", [validators.NumberRange(min=1)])
  quantity = IntegerField("Quantity", [validators.NumberRange(min=1)])
  type = SelectField("Type", choices=[("in", "Stock In"), ("out", "Stock Out")])
  remarks = StringField("Remarks", [validators.Length(max=50)])



class Stock(object):
  def stocks(self):
    headers = ["id","product","capital","srp","quantity","type","remarks","added_by","date_created"]

    query_str = "SELECT * FROM stocks WHERE date_deleted=\"0000-00-00 00:00:00\" ORDER BY id DESC"

    data = cmn.fetch_all(query_str)

    # Total products
    query_str = "SELECT COUNT(*) AS total_products FROM products"
    _data = cmn.fetch(query_str)
    total_products = _data["data"]["total_products"] if _data["status"] == "success" else 0

    # Total stocks
    query_str = "SELECT (SELECT SUM(quantity) FROM stocks WHERE type = 'in') - (SELECT SUM(quantity) FROM stocks WHERE type = 'out') AS total_stocks"
    _data = cmn.fetch(query_str)
    total_stocks = _data["data"]["total_stocks"] if _data["status"] == "success" else 0


    if data["status"] == "success":
      data["data"] = cmn.get_added_by(data["data"])
      data["data"] = cmn.get_product_name(data["data"])
      data["subdata"] = {
        "total_products": total_products,
        "total_stocks": total_stocks
      }
      return render_template("stocks/stocks.html", headers=headers, data=data["data"], subdata=data["subdata"])
    else:
      return render_template("stocks/stocks.html", msg=data["msg"])

  def add_stock(self):
    # Initialize form
    form = StockForm(request.form)

    # Get products
    query_str = "SELECT id, name, quantity FROM products"

    # Execute query
    data = cmn.fetch_all(query_str)

    if data["status"] == "success":
      form.product_id.choices = [(product["id"], product["name"]) for product in data["data"]]
    else:
      return render_template("stocks/stocks.html", msg=data["msg"])

    # Check if request method is POST
    if request.method == "POST" and form.validate():
      product_id = form.product_id.data
      capital = form.capital.data
      srp = form.srp.data
      quantity = form.quantity.data
      type = form.type.data
      remarks = form.remarks.data

      # Insert stock
      query_str = "INSERT INTO stocks (product_id,capital,srp,quantity,type,remarks,added_by) VALUES (\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\")".format(product_id,capital,srp,quantity,type,remarks,session["id"])
      cmn.commit(query_str)

      # Check if stock in
      if type == "in":
        query_str = "UPDATE products SET quantity=quantity+{},capital={},srp={} WHERE id={}".format(quantity,capital,srp,product_id)
        cmn.commit(query_str)

      # Check if stock out
      if type == "out":
        query_str = "UPDATE products SET quantity=quantity-{},capital={},srp={} WHERE id={}".format(quantity,capital,srp,product_id)
        cmn.commit(query_str)

        # Check alert limit
        query_str = "SELECT name, quantity FROM products WHERE quantity <= alert_limit AND id={}".format(product_id)
        data = cmn.fetch(query_str)

        if data["status"] == "success":
          flash("{} quantity left: {}".format(data["data"]["name"], data["data"]["quantity"]), "danger")

      flash("Stock created!", "success")

      return redirect(url_for("stocks"))

    return render_template("stocks/add_stock.html", form=form)

  """
  Stocks should not be edited

  def edit_stock(self, id):
    query_str = "SELECT * FROM stocks WHERE id={}".format(id)
    data = cmn.fetch(query_str)

    if data["status"] == "success":
      stock = data["data"]
    else:
      return render_template("stocks/stocks.html", msg=data["msg"])

    form = StockForm(request.form)

    form.product_id.data = stock["product_id"]
    form.quantity.data = stock["quantity"]
    form.type.data = stock["type"]
    form.remarks.data = stock["remarks"]

    if request.method == "POST" and form.validate():
      product_id = request.form["product_id"]
      quantity = request.form["quantity"]
      type = request.form["type"]
      remarks = request.form["remarks"]

      query_str = "UPDATE stocks SET product_id=\"{}\",quantity=\"{}\",type=\"{}\",remarks=\"{}\" WHERE id={}".format(product_id,quantity,type,remarks,id)

      cmn.commit(query_str)

      flash("Stock updated!", "success")

      return redirect(url_for("stocks"))

    return render_template("stocks/edit_stock.html", form=form)
  """

  # Do not delete actual data of stock
  def delete_stock(self, id):
    # Get quantity of stock
    query_str = "SELECT product_id, type, quantity FROM stocks WHERE id={}".format(id)
    data = cmn.fetch(query_str)

    product_id = data["data"]["product_id"]
    type = data["data"]["type"]
    quantity = data["data"]["quantity"]

    # Update date deleted
    query_str = "UPDATE stocks SET date_deleted=\"{}\" WHERE id={}".format(datetime.now(),id)
    cmn.commit(query_str)

    # Update product quantity
    if type == "in":
      query_str = "UPDATE products SET quantity=quantity-{} WHERE id={}".format(quantity,product_id)
    elif type == "out":
      query_str = "UPDATE products SET quantity=quantity+{} WHERE id={}".format(quantity,product_id)
    cmn.commit(query_str)

    flash("Stock Deleted", "success")
    return redirect(url_for("stocks"))

  # S T O C K   M O N I T O R I N G
  def stock_monitoring(self):

    # Get date today
    today = "{}".format(datetime.strftime(datetime.now(),"%Y-%m-%d"))

    # Get all products
    query_str = "SELECT id,name AS product,description FROM products"
    data = cmn.fetch_all(query_str)

    if data["status"] == "success":
      data = data["data"]
    else:
      return render_template("stocks/monitoring.html", msg=data["msg"])

    # Get beginning quantity of products
    products = []
    for d in data:
      # Get total stock in
      query_str = "SELECT SUM(quantity) AS stock_in FROM stocks WHERE date_deleted = 0 AND product_id={} AND type=\"in\" AND date_created < \"{}\"".format(d["id"],today)
      data = cmn.fetch(query_str)
      stock_in = data["data"]["stock_in"] if data["data"]["stock_in"] else 0

      # Get total stock out
      query_str = "SELECT SUM(quantity) AS stock_out FROM stocks WHERE date_deleted = 0 AND product_id={} AND type=\"out\" AND date_created < \"{}\"".format(d["id"],today)
      data = cmn.fetch(query_str)
      stock_out = data["data"]["stock_out"] if data["data"]["stock_out"] else 0

      # Get total sales
      #   Not yet prepared
      #   Add product id on ordered_products table to get total sales per product
      query_str = "SELECT SUM(a.quantity) sold FROM ordered_products a INNER JOIN orders b ON a.invoice_no = b.invoice_no WHERE a.product_id={} AND b.date_created < \"{}\"".format(d["id"],today)
      data = cmn.fetch(query_str)
      sold = data["data"]["sold"] if data["data"]["sold"] else 0

      temp = d
      temp["beginning"] = abs(stock_in - (stock_out + sold))

      products.append(temp)

    data = products

    # Get ending quantity and details of products to justify beginning and ending quantity
    products =[]
    for d in data:
      # Get total stock in
      query_str = "SELECT SUM(quantity) AS stock_in FROM stocks WHERE date_deleted = 0 AND product_id={} AND type=\"in\" AND date_created >= \"{}\"".format(d["id"],today)
      data = cmn.fetch(query_str)
      stock_in = data["data"]["stock_in"] if data["data"]["stock_in"] else 0

      # Get total stock out
      query_str = "SELECT SUM(quantity) AS stock_out FROM stocks WHERE date_deleted = 0 AND product_id={} AND type=\"out\" AND date_created >= \"{}\"".format(d["id"],today)
      data = cmn.fetch(query_str)
      stock_out = data["data"]["stock_out"] if data["data"]["stock_out"] else 0

      # Get total sales
      #   Not yet prepared
      #   Add product id on ordered_products table to get total sales per product
      query_str = "SELECT SUM(a.quantity) sold FROM ordered_products a INNER JOIN orders b ON a.invoice_no = b.invoice_no WHERE a.product_id={} AND b.date_created >= \"{}\"".format(d["id"],today)
      data = cmn.fetch(query_str)
      sold = data["data"]["sold"] if data["data"]["sold"] else 0

      temp = d
      temp["ending"] = abs((stock_in - (stock_out + sold)) + d["beginning"])
      temp["stock_in"] = stock_in
      temp["stock_out"] = stock_out
      temp["sold"] = sold

      products.append(temp)

    headers = ["id","product","description","beginning","stock_in","stock_out","sold","ending"]

    return render_template("stocks/monitoring.html", headers=headers, data=products)
