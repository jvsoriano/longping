from flask import Flask, render_template, flash, redirect, url_for, session, logging, request
from wtforms import Form, StringField, IntegerField, TextAreaField, PasswordField, SelectField, validators
from passlib.hash import sha256_crypt
from datetime import datetime
from lib import common
import json

cmn = common.Common()

class Pos(object):
  def pos(self):
    if request.method == "POST":
      # Parse form data
      invoice_no = request.form["invoice_no"]
      customer_name = request.form["customer_name"]
      customer_total = request.form["customer_total"]
      customer_payment = request.form["customer_payment"]
      customer_change = request.form["customer_change"]

      # Insert order
      query_str = """
        INSERT INTO orders (invoice_no,customer,amount,payment,payment_change,added_by) 
        VALUES (\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\")""".format(
          invoice_no,customer_name,customer_total,
          customer_payment,customer_change,session["id"]
        )
      cmn.commit(query_str)

      # Insert ordered products
      query_chunk = []
      product_id_list = []

      # Get products
      for key in request.form:
        if key in ["invoice_no","customer_name","customer_total","customer_payment","customer_change"]:
          continue
        else:
          product_id = key.split("-")[1]
          if not product_id in product_id_list:
            product_id_list.append(product_id)
      
      for product_id in product_id_list:
        product_dict = {}
        
        for key in request.form:
          if not product_id in key:
            continue
          product_key = key.split("-")[0]
          product_dict[product_key] = request.form[key]

        product_dict["amount"] = float(product_dict["srp"]) * float(product_dict["quantity"])
        query_chunk.append("(\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\")".format(
          product_id,invoice_no,product_dict["name"],product_dict["srp"],
          product_dict["quantity"],product_dict["amount"]))

        # Update product quantity
        query_str = "UPDATE products SET quantity=quantity-{} WHERE id={}".format(product_dict["quantity"],product_id)
        cmn.commit(query_str)

      query_str = "INSERT INTO ordered_products (product_id,invoice_no,name,srp,quantity,amount) VALUES {}".format(",".join(query_chunk))
      cmn.commit(query_str)

      # Reload products
      query_str = "SELECT id,name,description,srp,quantity FROM products"
      data = cmn.fetch_all(query_str)

      if data["status"] == "success":
        return render_template("pos/pos.html", products=data["data"], msg="Order has been paid.")
      else:
        return render_template("pos/pos.html", msg=data["msg"])

    query_str = "SELECT id,name,description,srp,quantity FROM products"
    data = cmn.fetch_all(query_str)

    if data["status"] == "success":
      return render_template("pos/pos.html", products=data["data"])
    else:
      return render_template("pos/pos.html", msg=data["msg"])
     