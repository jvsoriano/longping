from flask import Flask, render_template, flash, redirect, url_for, session, logging, request
from wtforms import Form, StringField, IntegerField, TextAreaField, PasswordField, SelectField, validators
from passlib.hash import sha256_crypt
from datetime import datetime
from lib import common
import json

cmn = common.Common()

class SalesOrderForm(Form):
  sales_order_no = StringField('Sales Order No.', [validators.Length(min=1, max=6)])
  company_name = StringField('Company Name', [validators.Length(min=1, max=100)])
  customer_name = StringField('Customer Name', [validators.Length(min=1, max=100)])
  address_tin = StringField('Address/TIN', [validators.Length(min=1, max=100)])
  deliver_to = StringField('Deliver to (Address)', [validators.Length(min=1, max=100)])
  sales_order_date = StringField('S.O. Date', [validators.Length(min=0, max=100)])
  date_needed = StringField('Date Needed', [validators.Length(min=0, max=100)])
  terms = StringField('Terms', [validators.Length(min=1, max=100)])
  business_style = StringField('Bus. Style', [validators.Length(min=0, max=100)])
  osca_pwd_id_no = StringField('OSCA/PWD ID No.', [validators.Length(min=0, max=100)])
  sc_pwd_signature = StringField('SC/PWD Signature', [validators.Length(min=0, max=100)])

class SalesOrder(object):
  def sales_order(self):
    query_str = "SELECT id,sales_order_no,company_name,customer_name,address_tin,deliver_to,sales_order_date,date_needed,terms,business_style,osca_pwd_id_no,sc_pwd_signature FROM sales_order"
    data = cmn.fetch_all(query_str)

    if data["status"] == "success":
      return render_template("sales_order/sales_order.html", data=data["data"])
    else:
      return render_template("sales_order/sales_order.html", msg=data["msg"])

  def add_sales_order(self):
    form = SalesOrderForm(request.form)
    if request.method == "POST" and form.validate():
      sales_order_no = form.sales_order_no.data
      company_name = form.company_name.data
      customer_name = form.customer_name.data
      address_tin = form.address_tin.data
      deliver_to = form.deliver_to.data
      sales_order_date = form.sales_order_date.data
      date_needed = form.date_needed.data
      terms = form.terms.data
      business_style = form.business_style.data
      osca_pwd_id_no = form.osca_pwd_id_no.data
      sc_pwd_signature = form.sc_pwd_signature.data

      query_str = "INSERT INTO sales_order (sales_order_no,company_name,customer_name,address_tin,deliver_to,sales_order_date,date_needed,terms,business_style,osca_pwd_id_no,sc_pwd_signature) VALUES (\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\")".format(sales_order_no,company_name,customer_name,address_tin,deliver_to,sales_order_date,date_needed,terms,business_style,osca_pwd_id_no,sc_pwd_signature)
      cmn.commit(query_str)

      flash("Salse Order created!", "success")

      return redirect(url_for("sales_order"))

    return render_template("sales_order/add_sales_order.html", form=form)
     