from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, current_app
from wtforms import Form, StringField, IntegerField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from lib import common

app = Flask(__name__)

cmn = common.Common()

class ProductForm(Form):
  name = StringField('Name', [validators.Length(min=1, max=50)])
  description = StringField('Description', [validators.Length(min=4, max=25)])
  alert_limit = IntegerField('Alert limit', [validators.NumberRange(min=0)])

class Product(object):
  def products(self):
    query_str = "SELECT * FROM products"

    data = cmn.fetch_all(query_str)


    if data["status"] == "success":
      data["data"] = cmn.get_added_by(data["data"])
      return render_template("products/products.html", products=data["data"])
    else:
      return render_template("products/products.html", msg=data["msg"])

  def add_product(self):
    form = ProductForm(request.form)
    if request.method == "POST" and form.validate():
      name = form.name.data
      description = form.description.data
      alert_limit = form.alert_limit.data

      query_str = "INSERT INTO products (name,description,alert_limit,added_by) VALUES (\"{}\",\"{}\",\"{}\",\"{}\")".format(name,description,alert_limit,session["id"])
      cmn.commit(query_str)

      flash("Product created!", "success")

      return redirect(url_for("products"))

    return render_template("products/add_product.html", form=form)

  def edit_product(self, id):
    query_str = "SELECT * FROM products WHERE id={}".format(id)
    data = cmn.fetch(query_str)

    if data["status"] == "success":
      product = data["data"]
    else:
      return render_template("products/products.html", msg=data["msg"])

    form = ProductForm(request.form)

    form.name.data = product["name"]
    form.description.data = product["description"]
    form.alert_limit.data = product["alert_limit"]

    if request.method == "POST" and form.validate():
      name = request.form["name"]
      description = request.form["description"]
      alert_limit = request.form["alert_limit"]

      query_str = "UPDATE products SET name=\"{}\",description=\"{}\",alert_limit=\"{}\" WHERE id={}".format(name,description,alert_limit,id)

      cmn.commit(query_str)

      flash("Product updated!", "success")

      return redirect(url_for("products"))

    return render_template("products/edit_product.html", form=form)

  def delete_product(self, id):
    query_str = "DELETE FROM products WHERE id={}".format(id)
    cmn.commit(query_str)

    flash("Product Deleted", "success")
    return redirect(url_for("products"))